package me.xxmatthdxx.balloons;

import me.xxmatthdxx.balloons.listeners.ClickEvent;
import me.xxmatthdxx.balloons.listeners.JoinEvent;
import me.xxmatthdxx.balloons.listeners.MoveEvent;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Matthew on 2015-10-07.
 */
public class Balloons extends JavaPlugin {

    private static Balloons plugin;

    private HashMap<String, List<Entity
                >> packetMap = new HashMap<>();

    public HashMap<String, List<Entity>> getPacketMap(){
        return packetMap;
    }

    public void onEnable(){
        plugin = this;
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new ClickEvent(), this);
        pm.registerEvents(new MoveEvent(), this);
        pm.registerEvents(new JoinEvent(), this);
    }

    public void onDisable(){

    }

    public static Balloons getPlugin(){
        return plugin;
    }
}
