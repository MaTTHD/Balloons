package me.xxmatthdxx.balloons.listeners;

import me.xxmatthdxx.balloons.Balloons;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matthew on 2015-10-08.
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onJoin(PlayerQuitEvent e) {
        Player pl = e.getPlayer();
        if (Balloons.getPlugin().getPacketMap().containsKey(pl.getName())) {
            for (Entity e1 : Balloons.getPlugin().getPacketMap().get(pl.getName())) {
                e1.remove();
            }
            Balloons.getPlugin().getPacketMap().remove(pl.getName());
        }
    }
}
