package me.xxmatthdxx.balloons.listeners;

import me.xxmatthdxx.balloons.Balloons;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;


/**
 * Created by Matthew on 2015-10-07.
 */
public class MoveEvent implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e){

        Player pl = e.getPlayer();

        if(Balloons.getPlugin().getPacketMap().containsKey(pl.getName())){
            ArmorStand stand = (ArmorStand) Balloons.getPlugin().getPacketMap().get(pl.getName()).get(0);

            Vector a = stand.getLocation().toVector();
            Vector b = pl.getLocation().add(0,5,0).toVector();
            Vector direction = b.subtract(a).normalize();

            if(pl.isFlying()){
                stand.setVelocity(direction.multiply(pl.getFlySpeed()));
            }
            else {
                stand.setVelocity(direction.multiply(pl.getWalkSpeed()));
            }
        }
    }
}
