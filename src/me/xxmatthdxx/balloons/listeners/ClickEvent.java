package me.xxmatthdxx.balloons.listeners;

import me.xxmatthdxx.balloons.Balloons;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2015-10-07.
 */
public class ClickEvent implements Listener {

    private Balloons plugin = Balloons.getPlugin();

    @EventHandler
    public void onClick(final PlayerInteractEvent e) {
        if (!(e.getItem().getType() == Material.LEASH)) {
            return;
        }

        if (e.getAction() != Action.RIGHT_CLICK_AIR) {
            return;
        }

        final Player pl = e.getPlayer();
        final World world = pl.getWorld();

        if (Balloons.getPlugin().getPacketMap().containsKey(pl.getName())) {
            for(Entity e1 : Balloons.getPlugin().getPacketMap().get(pl.getName())){
                e1.remove();
            }
            Balloons.getPlugin().getPacketMap().remove(pl.getName());
            pl.sendMessage(ChatColor.GREEN + "Toggled off balloon gadget.");
            world.playSound(pl.getLocation(), Sound.CHICKEN_EGG_POP, 1,1);
            world.playEffect(pl.getLocation(), Effect.EXTINGUISH, 1,1);
        } else {

            final ArmorStand stand = world.spawn(pl.getLocation(), ArmorStand.class);
            stand.setLeashHolder(pl);
            System.out.println(stand.isLeashed());
            stand.setVisible(false);
            stand.setHelmet(new ItemStack(Material.WOOL, 1, (byte) 4));
            MagmaCube cube = world.spawn(stand.getEyeLocation(), MagmaCube.class);
            cube.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 2));
            stand.setPassenger(cube);
            cube.setLeashHolder(pl);

            List<Entity> usedEntities = new ArrayList<>();
            usedEntities.add(stand);
            usedEntities.add(cube);
            Balloons.getPlugin().getPacketMap().put(pl.getName(), usedEntities);
            pl.sendMessage(ChatColor.GREEN + "Toggled on balloon gadget.");

            new BukkitRunnable() {
                public void run() {
                    if (plugin.getPacketMap().containsKey(pl.getName())) {
                        doFlying(pl, stand);
                    } else {
                        cancel();
                    }
                }
            }.runTaskTimer(plugin, 0L, 5L);
        }
    }

    public void doFlying(Player pl, ArmorStand stand) {
        if (stand.getLocation().getY() < pl.getLocation().add(0, 5, 0).getY()) {
            stand.setVelocity(new Vector(0, 0.25, 0));
        } else {
            stand.setVelocity(new Vector(0, 0.01, 0));
        }
    }
}

