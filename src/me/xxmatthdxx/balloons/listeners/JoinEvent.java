package me.xxmatthdxx.balloons.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Matthew on 2015-10-08.
 */
public class JoinEvent implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player pl = e.getPlayer();

        ItemStack item  = new ItemStack(Material.LEASH,1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "Balloon Toggle");
        item.setItemMeta(meta);
        pl.getInventory().setItem(0, item);
        pl.getInventory().setHeldItemSlot(0);
    }
}
